package com.hindsighttesting.cucumber.example;

import org.junit.runner.RunWith;

import cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(format = { "json", "json:target/cucumber.json" }, strict = true)
public class CucumberIT {

}
